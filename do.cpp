#include <iostream>
#include <sqlite3.h>

int main() {
    sqlite3* db;
    int rc = sqlite3_open("database.db", &db); // Подключение к базе данных

    if (rc) {
        std::cerr << "Ошибка при открытии базы данных: " << sqlite3_errmsg(db) << std::endl;
        return rc;
    }

    const char* sql = "SELECT * FROM table_name"; // Запрос на выборку всех данных из таблицы
    sqlite3_stmt* stmt;

    rc = sqlite3_prepare_v2(db, sql, -1, &stmt, nullptr); // Подготовка запроса

    if (rc != SQLITE_OK) {
        std::cerr << "Ошибка при подготовке запроса: " << sqlite3_errmsg(db) << std::endl;
        return rc;
    }

    std::cout << "[";

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        int columnCount = sqlite3_column_count(stmt);

        std::cout << "{";
        for (int i = 0; i < columnCount; i++) {
            const char* columnName = sqlite3_column_name(stmt, i);
            std::cout << "\"" << columnName << "\": ";

            switch (sqlite3_column_type(stmt, i)) {
                case SQLITE_INTEGER:
                    std::cout << sqlite3_column_int(stmt, i);
                    break;
                case SQLITE_FLOAT:
                    std::cout << sqlite3_column_double(stmt, i);
                    break;
                case SQLITE_TEXT:
                    std::cout << "\"" << sqlite3_column_text(stmt, i) << "\"";
                    break;
                case SQLITE_NULL:
                    std::cout << "null";
                    break;
                default:
                    break;
            }

            if (i < columnCount - 1) {
                std::cout << ", ";
            }
        }
        std::cout << "}";

        if (sqlite3_column_count(stmt) > 0 && sqlite3_column_count(stmt) != columnCount) {
            std::cout << ", ";
        }
    }

    std::cout << "]";

    sqlite3_finalize(stmt); // Освобождение ресурсов запроса
    sqlite3_close(db); // Закрытие базы данных

    return 0;
}
